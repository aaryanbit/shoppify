from django.db import models
from django.contrib.auth.models import User

# Create your models here.
from django.db.models import FloatField


class Customer(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=100)
    locality=models.CharField(max_length=100)
    city=models.CharField(max_length=100)
    state=models.CharField(max_length=100)
    zipcode=models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = "Customers"
        db_table = 'app_customer'


class Product(models.Model):
    title = models.CharField(max_length=250)
    selling_price: FloatField = models.FloatField()
    discounted_price = models.FloatField()
    brand = models.CharField(max_length=200)
    description = models.TextField(max_length=200)
    category = models.CharField(max_length=200)
    product_image = models.ImageField(upload_to="app/images", default="")

    def __str__(self):
        return self.brand

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
        db_table = 'app_product'


class OrderPlaced(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    quantity=models.PositiveIntegerField()
    ordered_date=models.DateTimeField()
    status=models.CharField(max_length=200)

    def __str__(self):
        return self.status

    class Meta:
        verbose_name="Orderplaced"
        verbose_name_plural="Orderplacedes"
        db_table='app_order_placed'
class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity=models.PositiveIntegerField()
    def __str__(self):
        return self.quantity
    class Meta:
        verbose_name="Cart"
        verbose_name_plural="Carts"
        db_table='app_cart'








