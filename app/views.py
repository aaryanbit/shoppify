from django.shortcuts import render, HttpResponse
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from app.models import Product


# Create your views here
@csrf_exempt
def user_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        print(user)
        if user is not None:
            login(request, user)
            return HttpResponse("Successfully logged In.")
        else:
            return HttpResponse("Something went wrong!")


@csrf_exempt
def register(request):
    if request.method == "POST":
        username = request.POST.get("username", None)
        email = request.POST.get("email", None)
        password = request.POST.get("password", None)

        user = User.objects.create_user(username=username, email=email, password=password)

        if user:
            user.is_active = True
            return HttpResponse("Successfully registered.")
        else:
            return HttpResponse("something went wrong!")


@csrf_exempt
def product(request):
    if request.method == "POST":
        title = request.POST.get("title", None)
        selling_price = request.POST.get("selling_price", None)
        discounted_price = request.POST.get("discounted_price", None)
        brand = request.POST.get("brand", None)
        description = request.POST.get("description", None)
        category = request.POST.get("category", None)
        product_image = request.POST.get("product_image", None)

        p = Product()
        p.title = title
        p.selling_price = selling_price
        p.discounted_price = discounted_price
        p.brand = brand
        p.description = description
        p.catq = category
        p.product_image = product_image

        p.save()

        return HttpResponse("Product added successfully.")

    else:
        return HttpResponse("Method not allowed.")


def home(request):
 return render(request, 'app/home.html')

def product_detail(request):
 return render(request, 'app/productdetail.html')

def add_to_cart(request):
 return render(request, 'app/addtocart.html')

def buy_now(request):
 return render(request, 'app/buynow.html')

def profile(request):
 return render(request, 'app/profile.html')

def address(request):
 return render(request, 'app/address.html')

def orders(request):
 return render(request, 'app/orders.html')

def change_password(request):
 return render(request, 'app/changepassword.html')

def mobile(request):
 return render(request, 'app/mobile.html')

def login(request):
 return render(request, 'app/login.html')

def customerregistration(request):
 return render(request, 'app/customerregistration.html')

def checkout(request):
 return render(request, 'app/checkout.html')
